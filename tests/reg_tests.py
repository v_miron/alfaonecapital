import pytest
from faker import Faker
from selenium.webdriver.common.by import By

from alfaonecapital.pages.reg_page import RegistrationPage


MENU_XPATH = "//div/div[1]/ul"
MSG_DIFFERENT_PASS = "//li[contains(text(), 'Поле Пароль не совпадает с подтверждением.')]"


class TestRegistration:
    """Checking positive and negative cases for registration page"""

    def test_positive(self, driver):
        """Verify that user can register with all valid data"""
        rp = RegistrationPage(driver)
        fake = Faker()
        rp.open_reg_page()
        rp.input_name(fake.first_name())
        rp.input_last_name(fake.last_name())
        rp.input_email(fake.email())
        rp.input_phone(fake.phone_number())
        rp.input_password(fake.password(
            length=20, special_chars=False, upper_case=False))
        rp.accept_terms()
        rp.click_reg_btn()
        user_menu = rp.wait_element(By.XPATH, MENU_XPATH)
        assert user_menu
        rp.exit_from_cabinet()

    def test_reg_without_accepting_terms(self, driver):
        """Verify that user can't register without accepting terms"""
        rp = RegistrationPage(driver)
        fake = Faker()
        rp.open_reg_page()
        rp.input_name(fake.first_name())
        rp.input_last_name(fake.last_name())
        rp.input_email(fake.email())
        rp.input_phone(fake.phone_number())
        rp.input_password(fake.password(
            length=20, special_chars=False))
        rp.click_reg_btn()
        current_url = rp.get_current_url()
        assert current_url == rp.base_url

    def test_different_passwords(self, driver):
        """Verify that user can't register if password
        and confirm password are different"""
        rp = RegistrationPage(driver)
        fake = Faker()
        pass1 = fake.password(
            length=20, special_chars=False, upper_case=False)
        pass2 = fake.password(
            length=20, special_chars=False, upper_case=False)
        rp.open_reg_page()
        rp.input_name(fake.first_name())
        rp.input_last_name(fake.last_name())
        rp.input_email(fake.email())
        rp.input_phone(fake.phone_number())
        rp.input_different_passwords(pass1, pass2)
        rp.accept_terms()
        rp.click_reg_btn()
        error_msg = rp.wait_element(By.XPATH, MSG_DIFFERENT_PASS)
        assert error_msg

    def test_reg_without_email(self, driver):
        """Verify that user can't register without email"""
        rp = RegistrationPage(driver)
        fake = Faker()
        rp.open_reg_page()
        rp.input_name(fake.first_name())
        rp.input_last_name(fake.last_name())
        rp.input_phone(fake.phone_number())
        rp.input_password(fake.password(
            length=20, special_chars=False))
        rp.accept_terms()
        rp.click_reg_btn()
        current_url = rp.get_current_url()
        assert current_url == rp.base_url

    def test_reg_without_phone(self, driver):
        """Verify that user can't register without phone number"""
        rp = RegistrationPage(driver)
        fake = Faker()
        rp.open_reg_page()
        rp.input_name(fake.first_name())
        rp.input_last_name(fake.last_name())
        rp.input_email(fake.email())
        rp.input_password(fake.password(
            length=20, special_chars=False, upper_case=False))
        rp.accept_terms()
        rp.click_reg_btn()
        current_url = rp.get_current_url()
        assert current_url == rp.base_url

    def test_reg_without_first_name(self, driver):
        """Verify that user can't register without first name"""
        rp = RegistrationPage(driver)
        fake = Faker()
        rp.open_reg_page()
        rp.input_last_name(fake.last_name())
        rp.input_email(fake.email())
        rp.input_phone(fake.phone_number())
        rp.input_password(fake.password(
            length=20, special_chars=False, upper_case=False))
        rp.accept_terms()
        rp.click_reg_btn()
        current_url = rp.get_current_url()
        assert current_url == rp.base_url

    def test_reg_without_last_name(self, driver):
        """Verify that user can't register without last name"""
        rp = RegistrationPage(driver)
        fake = Faker()
        rp.open_reg_page()
        rp.input_name(fake.first_name())
        rp.input_email(fake.email())
        rp.input_phone(fake.phone_number())
        rp.input_password(fake.password(
            length=20, special_chars=False, upper_case=False))
        rp.accept_terms()
        rp.click_reg_btn()
        current_url = rp.get_current_url()
        assert current_url == rp.base_url
