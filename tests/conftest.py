import pytest
from selenium import webdriver
from selenium.webdriver import FirefoxOptions


@pytest.fixture(scope="class")
def driver():
    """Assigning and Returning a Driver"""
    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(executable_path='C:/Users/vmiro/geckodriver.exe', options=options)
    yield driver
    driver.quit()
