from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://dev.devse.xyz/register"

    def wait_element(self, by_type, locator):
        wait = WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located((by_type, locator)),
            message=f"Cant find element by locator {locator}"
        )
        return wait

    def get_current_url(self):
        return self.driver.current_url
