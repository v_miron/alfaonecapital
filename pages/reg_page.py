from selenium.webdriver.common.by import By

from alfaonecapital.pages.base_page import BasePage


NAME_FIELD = "//input[@name='first_name']"
LAST_NAME_FIELD = "//input[@name='last_name']"
EMAIL_FIELD = "//input[@name='email']"
PHONE_FIELD = "//input[@name='phone']"
PASSWORD_FIELD = "//input[@name='password']"
PASSWORD_CONFIRM_FIELD = "//input[@name='password_confirmation']"
TERMS_CHECKBOX = "//input[@name='terms']"
CONFIRM_BTN = "//button[contains(text(), 'Регистрация')]"
ALREADY_REGISTERED_LINK = "//a[contains(text(), 'Уже зарегистрированы?')]"
EXIT_BTN = "//span[contains(text(), 'Выйти')]"


class RegistrationPage(BasePage):

    def open_reg_page(self):
        self.driver.get(self.base_url)

    def input_name(self, data=""):
        name_field = self.wait_element(By.XPATH, NAME_FIELD)
        name_field.send_keys(data)

    def input_last_name(self, data=""):
        last_name_field = self.wait_element(By.XPATH, LAST_NAME_FIELD)
        last_name_field.send_keys(data)

    def input_email(self, data=""):
        email_field = self.wait_element(By.XPATH, EMAIL_FIELD)
        email_field.send_keys(data)

    def input_phone(self, data=""):
        phone_field = self.wait_element(By.XPATH, PHONE_FIELD)
        phone_field.send_keys(data)

    def input_password(self, data=""):
        password_field = self.wait_element(By.XPATH, PASSWORD_FIELD)
        password_field.send_keys(data)
        password_confirm_field = self.wait_element(By.XPATH, PASSWORD_CONFIRM_FIELD)
        password_confirm_field.send_keys(data)

    def input_different_passwords(self, pass1, pass2):
        password_field = self.wait_element(By.XPATH, PASSWORD_FIELD)
        password_field.send_keys(pass1)
        password_confirm_field = self.wait_element(By.XPATH, PASSWORD_CONFIRM_FIELD)
        password_confirm_field.send_keys(pass2)

    def accept_terms(self):
        terms_checkbox = self.wait_element(By.XPATH, TERMS_CHECKBOX)
        terms_checkbox.click()

    def click_reg_btn(self):
        reg_btn = self.wait_element(By.XPATH, CONFIRM_BTN)
        reg_btn.click()

    def exit_from_cabinet(self):
        self.wait_element(By.XPATH, EXIT_BTN).click()
